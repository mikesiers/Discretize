var assert = require('assert');
var fs = require('fs');
var JSDataset = require('../lib/JSDataset.js');
var Discretize = require('../Discretize.js');

// Read in a dataset for testing.
var adult_file_string = fs.readFileSync('TestData/adult.arff', 'utf8');
var adult_dataset = JSDataset.parse_ARFF(adult_file_string);

changes_data_type = function() {
	it('Changes data type of specified attribute to categorical', function() {
		target_attribute = this.dataset.get_attribute(1)
		assert(target_attribute instanceof JSDataset.Categorical_Attribute) 
	});
};
