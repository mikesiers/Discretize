# Discretize
Discretization library for data science in the browser. Uses client side javascript.

# Dev-strategy
- Class diagrams are developed first.
- Test Driven Development (Using Mocha).
- JSDoc used to generate clear documentation.
- Original base code (GenericDiscretizer) developed on master.
- Other code (discretization algorithms) are developed on branches.
- All code is provided in the one file Discretize.js. 
