var JSDataset = require('../lib/JSDataset.js');

/**
 * @class
 * @classdesc An abstract class which holds the basic functionality for
 * Discretization classes.
 */
var Generic_Discretizer = function() {
};

/**
 * A virtual method which returns the value of the value object.
 */
Generic_Discretizer.prototype.discretize = function(dataset, index) {
	var attribute = dataset.get_attribute(index);

	// Return if the attribute is already Categorical.
	if (attribute instanceof JSDataset.Categorical_Attribute) {
		return;
	}

	// Change the class of the attribute to Categorical_Attribute
	var name = attribute.get_name();
	var new_attribute = new JSDataset.Categorical_Attribute(name, [], dataset);
};
